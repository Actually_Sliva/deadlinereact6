import { addProductsAction } from "../../redux/actions";
import { productsReducer } from "../../redux/reducers/productsReducer";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

test("should store products", async () => {
    const productsArr = [
        {
            name: "Fostex TH900 mkII",
            price: 58140,
            imgUrl: "https://era-in-ear.com/wp-content/uploads/2016/03/th900mk2-1-e1668336409969-1500x1500.webp",
            artNum: 10756,
            color: "red"
        },
        {
            name: "Fostex TH610",
            price: 21280,
            imgUrl: "https://era-in-ear.com/wp-content/uploads/shop/products/origin/4c74d51dfd633243b38d6c5435b4d9bd-1024x1024.jpg",
            artNum: 11081,
            color: "brown"
        },
    ]
    const initialState = { products: [] };

    const action = addProductsAction(productsArr)
    const newState = productsReducer(initialState, action)

    expect(newState.products).toEqual(productsArr)
})
