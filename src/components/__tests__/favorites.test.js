import { cleanup } from "@testing-library/react";
import { setFavorite } from "../../redux/actions";
import { setFavoritesAction } from "../../redux/actions";
import { favoritesReducer } from "../../redux/reducers/favoritesReducer";

afterEach(cleanup);

const product = {
    name: "Fostex TH610",
    price: 21280,
    imgUrl: "https://era-in-ear.com/wp-content/uploads/shop/products/origin/4c74d51dfd633243b38d6c5435b4d9bd-1024x1024.jpg",
    artNum: 11081,
    color: "brown"
}

test("should add product to favourites, just action", () => {
    const initialState = { favorites: [] }
    const action = setFavoritesAction([product])
    const newState = favoritesReducer(initialState, action)

    expect(newState.favorites).toEqual([product])
});

test("should add product to favourites", () => {
    const initialState = { favorites: [] }
    const dispatch = jest.fn()
    const action = setFavorite([], product)

    action(dispatch)
    
    const newState = favoritesReducer(initialState, dispatch.mock.calls[0][0])

    expect(newState.favorites).toEqual([product])
});

test("should delete product from favourites", () => {
    const initialState = { favorites: [product] }
    const action = setFavorite([product], product)
    const newState = favoritesReducer(initialState, action)

    expect(newState.favorites).toEqual([product])
});