import style from './cart.module.scss'
import Container from '../../components/Container';
import ProductList from '../../components/ProductList';
import { useSelector } from "react-redux";
import CartForm from '../../components/CartForm';

export function Cart() {

    const cart = useSelector(state => state.cart.cart)
    const loading = useSelector(state => state.loading.loading)
    const error = useSelector(state => state.error.error)

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    const uniqueArtNums = [...new Set(cart.map(item => item.artNum))]
    const uniqueCartProducts = uniqueArtNums.map(item => cart.find(product => product.artNum === item))

    return (
        <Container>
            {!loading
                ? uniqueCartProducts.length
                    ? <div className={style.wrapper}>
                        <ProductList products={uniqueCartProducts} type={'delete'} productAmount={true} />
                        <CartForm />
                    </div>
                    : <p className={style.empty}>Ви ще нічого не додали в кошик</p>
                : <p className={style.empty}>Йде завантаження...</p>}
        </Container>
    )
}