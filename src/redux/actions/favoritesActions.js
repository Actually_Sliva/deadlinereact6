import { favoritesTypes } from "../types/favoritesTypes";

export function setFavorite(favorites, product) {
    if (favorites.find(favorite => favorite.artNum === product.artNum)) {
        const newFavorites = favorites.filter(item => item.artNum !== product.artNum)

        localStorage.setItem('favorites', JSON.stringify(newFavorites))
        return dispatch => dispatch(setFavoritesAction(newFavorites))
    } else {
        const newFavorites = [...favorites, product]

        localStorage.setItem('favorites', JSON.stringify(newFavorites))
        return dispatch => dispatch(setFavoritesAction(newFavorites))
    }
}

export function setFavoritesAction(payload) {
    return {
        type: favoritesTypes.SET_FAVORITES,
        payload
    }
}